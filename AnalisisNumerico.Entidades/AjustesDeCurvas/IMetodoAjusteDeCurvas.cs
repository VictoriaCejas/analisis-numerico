﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisisNumerico.Entidades.AjustesDeCurvas
{
    public interface IMetodoAjusteDeCurvas
    {
        ResultadoAjusteDeCurvas MetodoRegresionLineal(ParametrosRegresionLineal parametros);
        ResultadoAjusteDeCurvas MetodoRegresionPolinomial(ParametrosRegresionPolinomial parametros);
        ResultadoLagrange MetodoLagrange(ParametrosLagrange parametros);
    }
}
