﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisisNumerico.Entidades.AjustesDeCurvas
{
    public class ParametrosAjustesDeCurvas
    {
        public double[,] Tabla { get; set; }
    }
}
