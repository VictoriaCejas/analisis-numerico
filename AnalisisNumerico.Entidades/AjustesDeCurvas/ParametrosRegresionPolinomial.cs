﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisisNumerico.Entidades.AjustesDeCurvas
{
    public class ParametrosRegresionPolinomial:ParametrosAjustesDeCurvas
    {
        public int Grado { get; set; }
    }
}
