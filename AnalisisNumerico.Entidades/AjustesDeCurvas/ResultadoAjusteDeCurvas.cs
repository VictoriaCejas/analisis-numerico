﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisisNumerico.Entidades.AjustesDeCurvas
{
    public class ResultadoAjusteDeCurvas
    {
        public double CoeficienteCorrelacion { get; set; }
        public double[] valores {get; set;}
    }
}
