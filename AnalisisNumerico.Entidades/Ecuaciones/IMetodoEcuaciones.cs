﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisisNumerico.Entidades.Ecuaciones
{
    public interface IMetodoEcuaciones
    {
        ResultadoGaussJordan MetodoGaussJordan(ParametrosGaussJordan parametros);
        ResultadoGaussSeidel MetodoGaussSeidel(ParametrosGaussSeidel parametros);
    }
}
