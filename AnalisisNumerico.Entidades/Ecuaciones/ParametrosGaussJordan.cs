﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisisNumerico.Entidades.Ecuaciones
{
    public class ParametrosGaussJordan
    {
        public double[,] Matriz { get; set; }
        public int CantidadIncognitas { get; set; }
    }
}
