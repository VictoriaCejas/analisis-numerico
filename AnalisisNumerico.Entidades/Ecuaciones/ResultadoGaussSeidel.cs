﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisisNumerico.Entidades.Ecuaciones
{
    public class ResultadoGaussSeidel: ResultadoGaussJordan
    {
        public double ErrorRelativo { get; set; }
        public int Iteraciones { get; set; }
    }
}
