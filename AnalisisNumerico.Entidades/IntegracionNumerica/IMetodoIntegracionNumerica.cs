﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisisNumerico.Entidades.IntegracionNumerica
{
    public interface IMetodoIntegracionNumerica
    {
        double MetodoTrapecio(ParametrosMetodosSimples parametros);
        double MetodoTrapecioMultiple(ParametrosMetodosMultiples parametros);
        double MetodoSimpson1_3Simple(ParametrosMetodosSimples parametros);
        double MetodoSimpson(ParametrosMetodosMultiples parametros);
    }
}
