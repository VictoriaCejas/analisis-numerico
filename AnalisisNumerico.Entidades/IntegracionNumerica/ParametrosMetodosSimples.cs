﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisisNumerico.Entidades.IntegracionNumerica
{
    public class ParametrosMetodosSimples
    {
        public string Funcion { get; set; }
        public double PuntoInicial { get; set; }
        public double PuntoFinal { get; set; }
    }
}
