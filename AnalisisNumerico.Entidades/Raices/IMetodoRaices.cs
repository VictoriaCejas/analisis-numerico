﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisisNumerico.Entidades.Raices
{
    public interface IMetodosRaices
    {
        ResultadoRaices MetodoBiseccion(ParametrosBiseccion parametros);
        ResultadoRaices MetodoReglaFalsa(ParametrosBiseccion parametros);
        ResultadoRaices MetodoTangente(ParametrosTangente parametros);
        ResultadoRaices MetodoSecante(ParametrosSecante parametros);
    }
}
