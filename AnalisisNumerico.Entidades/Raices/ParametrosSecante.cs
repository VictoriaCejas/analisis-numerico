﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisisNumerico.Entidades.Raices
{
    public class ParametrosSecante:ParametrosTangente
    {
        public double Xd { get; set; }
    }
}
