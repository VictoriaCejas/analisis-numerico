﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisisNumerico.Entidades
{
    public class ResultadoRaices
    {
        public string Raiz { get; set; }
        public int Iteraciones { get; set; }
        public string ErrorRelativo { get; set; }
    }
}
