﻿using org.mariuszgromada.math.mxparser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AnalisisNumerico.Logica
{
    public class UtilityFunctions : IUtilityFunctions
    {
        public double EvaluarFuncion(string nombreFuncion, Function funcion, Argument argument)
        {
            return (new Expression(nombreFuncion, funcion, argument)).calculate();
        }
    }
}
