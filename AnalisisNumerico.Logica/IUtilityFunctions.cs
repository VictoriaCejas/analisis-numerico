﻿using org.mariuszgromada.math.mxparser;

namespace AnalisisNumerico.Logica
{
    public interface IUtilityFunctions
    {
        double EvaluarFuncion(string nombreFuncion, Function funcion, Argument argument);
    }
}