﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnalisisNumerico.Entidades.AjustesDeCurvas;
using org.mariuszgromada.math.mxparser;
using System.Globalization;
using AnalisisNumerico.Entidades.Ecuaciones;

namespace AnalisisNumerico.Logica
{
    public class MetodoAjusteDeCurvas : IMetodoAjusteDeCurvas
    {
        private readonly IMetodoEcuaciones metodoEcuaciones;

        public MetodoAjusteDeCurvas(MetodoEcuaciones metodoEcuaciones)
        {
            this.metodoEcuaciones = metodoEcuaciones;
        }

        public ResultadoAjusteDeCurvas MetodoRegresionLineal(ParametrosRegresionLineal parametros)
        {
            var Tabla = parametros.Tabla;
            var sumX = 0.0;
            var sumY = 0.0;
            var sumX2 = 0.0;
            var xMed = 0.0;
            var yMed = 0.0;
            var producto = 0.0;
            var longitud = parametros.Tabla.GetLength(0);

            for (int i = 0; i < longitud; i++)
            {
                sumX = sumX + Tabla[i, 0];
                sumY = sumY + Tabla[i, 1];
                sumX2 = sumX2 + Math.Pow(Tabla[i, 0], 2);
                producto = producto + (Tabla[i, 0] * Tabla[i, 1]);
            }

            xMed = sumX / longitud;
            yMed = sumY / longitud;

            var a1 = ((longitud * producto) - (sumX * sumY)) / ((longitud * sumX2) - (Math.Pow(sumX, 2)));
            var a0 = yMed - (a1 * xMed);
            var st = 0.0;

            for (int i = 0; i < longitud; i++)
            {
                st = st + Math.Pow((Tabla[i, 1] - yMed), 2);
            }

            var calcularSr = CalcularDiferencias(parametros.Tabla, a0, a1);
            var coeficienteCorrelacion = (Math.Sqrt((st - calcularSr) / st)) * 100;
            double[] resultados = new double[2];

            resultados[0] = a0;
            resultados[1] = a1;

            return new ResultadoAjusteDeCurvas() { CoeficienteCorrelacion = coeficienteCorrelacion, valores = resultados };

        }


        public double CalcularDiferencias(double[,] matriz, double a0, double a1)
        {
            var cultura = CultureInfo.CreateSpecificCulture("en-US");
            var stringA0 = a0.ToString(cultura);
            var stringA1 = a1.ToString(cultura);
            var funcionString = "F(x,y)=(" + stringA1 + "*(x))+(" + stringA0 + ")-(y)";
            Function function = new Function(funcionString);
            var di = 0.0;
            var calculo = 0.0;

            for (int i = 0; i < matriz.GetLength(0); i++)
            {
                var valoresFuncion = "F(" + matriz[i, 0].ToString(cultura) + "," + matriz[i, 1].ToString(cultura) + ")";
                Expression expression = new Expression(valoresFuncion, function);
                calculo = Math.Pow(expression.calculate(), 2);
                di = di + calculo;
            }

            return di;
        }


        public ResultadoAjusteDeCurvas MetodoRegresionPolinomial(ParametrosRegresionPolinomial parametros)
        {
            var tabla = parametros.Tabla;
            var n = tabla.GetLength(0);
            var totalAElevar = parametros.Grado * 2;
            double[] xiElevados = new double[(parametros.Grado * 2) + 1];
            var aElevar = 0;
            xiElevados[0] = n;

            for (int i = 1; i <= totalAElevar; i++)
            {
                aElevar = aElevar + 1;
                var sumatoria = 0.0;

                for (int j = 0; j < n; j++)
                {
                    sumatoria = sumatoria + Math.Pow(tabla[j, 0], aElevar);
                }

                xiElevados[i] = sumatoria;
            }

            double[] sumatoriasYiXi = new double[(parametros.Grado + 1)];
            var sumY = 0.0;

            for (int i = 0; i < (parametros.Grado + 1); i++)
            {

                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        sumatoriasYiXi[i] = sumatoriasYiXi[i] + tabla[j, 1];
                        sumY = sumY + tabla[j, 1];
                    }
                    else
                    {
                        var x = tabla[j, 0];
                        var y = tabla[j, 1];
                        sumatoriasYiXi[i] = sumatoriasYiXi[i] + (tabla[j, 1] * Math.Pow(tabla[j, 0], i));
                    }
                }
            }

            var yProm = sumY / n;
            double[,] matrizArmada = new double[(parametros.Grado + 1), (parametros.Grado + 2)];

            for (int i = 0; i < (parametros.Grado + 1); i++)
            {
                for (int j = 0; j < (parametros.Grado + 2); j++)
                {
                    if (j == (parametros.Grado + 1))
                    {
                        matrizArmada[i, j] = sumatoriasYiXi[i];
                    }
                    else
                    {
                        matrizArmada[i, j] = xiElevados[(i + j)];
                    }
                }
            }

            ResultadoGaussJordan resultado = new ResultadoGaussJordan();
            resultado = metodoEcuaciones.MetodoGaussJordan(new ParametrosGaussJordan { CantidadIncognitas = (parametros.Grado + 1), Matriz = matrizArmada });

            var funcion = "F(";
            var noVaComa = resultado.Resultados.Length - 1;

            for (int i = 1; i < (resultado.Resultados.Length); i++)
            {
                funcion = funcion + "x" + (i.ToString());
                if (i != noVaComa)
                {
                    funcion = funcion + ",";
                }
                else
                {
                    funcion = funcion + ",y)=";
                }
            }

            CultureInfo culture = new CultureInfo("en-US");
            funcion = funcion + resultado.Resultados[0].ToString(culture);

            for (int i = 1; i < resultado.Resultados.Length; i++)
            {
                funcion = funcion + "+((" + resultado.Resultados[i].ToString(culture) + ")* (x" + i.ToString() + "))";
            }
            funcion = funcion + "-(y)";

            Function function = new Function(funcion);
            var sr = 0.0;
            var calculo = 0.0;

            for (int i = 0; i < n; i++)
            {
                var valoresFuncion = "F(";

                for (int j = 1; j < (resultado.Resultados.Length); j++)
                {
                    valoresFuncion = valoresFuncion + Math.Pow(tabla[i, 0], j).ToString(culture);
                    if (j != noVaComa)
                    {
                        valoresFuncion = valoresFuncion + ",";
                    }
                    else
                    {
                        valoresFuncion = valoresFuncion + "," + tabla[i, 1].ToString(culture) + ")";
                    }
                }

                Expression expression = new Expression(valoresFuncion, function);
                calculo = Math.Pow(expression.calculate(), 2);
                sr = sr + calculo;
            }

            var st = 0.0;

            for (int i = 0; i < n; i++)
            {
                st = st + Math.Pow((tabla[i, 1] - yProm), 2);
            }

            var correlacion = (Math.Sqrt(Math.Abs((st - sr) / st))) * 100;

            ResultadoAjusteDeCurvas resultadoPolinomial = new ResultadoAjusteDeCurvas() { CoeficienteCorrelacion = correlacion, valores = resultado.Resultados };

            return resultadoPolinomial;

        }


        public ResultadoLagrange MetodoLagrange(ParametrosLagrange parametros)
        {
            var cantidad = parametros.Tabla.GetLength(0);
            var x = parametros.X;
            var tabla = parametros.Tabla;
            var polinomio = 0.0;
            var polinomioString = "";
            var noVaSuma = cantidad - 1;

            for (int j = 0; j < cantidad; j++)
            {
                polinomioString = polinomioString + "(" + tabla[j, 1].ToString();
                var sum = 1.00;

                for (int i = 0; i < cantidad; i++)
                {
                    if (i != j)
                    {
                        polinomioString = polinomioString + "*(X -" + "(" + tabla[i, 0].ToString() + "))";
                        var resta = tabla[j, 0] - tabla[i, 0];
                        sum = sum * resta;
                    }

                }
                if (j != noVaSuma)
                {
                    polinomioString = polinomioString + "/(" + sum.ToString() + ")" + ")+";

                }
                else
                {
                    polinomioString = polinomioString + "/(" + sum.ToString() + ")" + ")";

                }
            }

            polinomio = 0.0;

            for (int j = 0; j < cantidad; j++)
            {
                var prod = tabla[j, 1];
                var productoria = 1.0;
                var sumaprod = 1.0;
                for (int i = 0; i < cantidad; i++)
                {
                    if (i != j)
                    {
                        productoria = (x - tabla[i, 0]) / (tabla[j, 0] - tabla[i, 0]);
                        sumaprod = sumaprod * productoria;
                    }
                }
                polinomio = polinomio + (sumaprod * prod);
            }

            return new ResultadoLagrange() { Polinomio = polinomioString, Resultado = polinomio };
        }
    }
}
