﻿using AnalisisNumerico.Entidades.Ecuaciones;
using org.mariuszgromada.math.mxparser;
using System;
using System.Globalization;

namespace AnalisisNumerico.Logica
{
    public class MetodoEcuaciones : IMetodoEcuaciones
    {

        #region GaussSeidel

        public ResultadoGaussSeidel MetodoGaussSeidel(ParametrosGaussSeidel parametros)
        {
            double[,] matriz = parametros.Matriz;
            string[] vectorIncognitas = new string[parametros.CantidadIncognitas];
            var fila = 0;
            var cultura = CultureInfo.CreateSpecificCulture("en-US");
            //Arma ecuaciones
            while (fila < parametros.CantidadIncognitas)
            {
                var div = "";
                vectorIncognitas[fila] = "(" + matriz[fila, parametros.CantidadIncognitas].ToString() + "-(";
                for (int j = 0; j < parametros.CantidadIncognitas; j++)
                {
                    if (j == fila)
                        div = matriz[fila, j].ToString(cultura);
                    else
                    {
                        if (matriz[fila, j] >= 0)
                        {
                            vectorIncognitas[fila] = vectorIncognitas[fila] + "+" + matriz[fila, j].ToString(cultura) + "*" + "X" + (j + 1).ToString(cultura);
                        }
                        else
                        {
                            vectorIncognitas[fila] = vectorIncognitas[fila] + matriz[fila, j].ToString(cultura) + "*" + "X" + (j + 1).ToString(cultura);
                        }
                    }
                }
                vectorIncognitas[fila] = vectorIncognitas[fila] + "))" + "/" + div;

                fila = fila + 1;
            }

            double[] solucionAnterior = new double[parametros.CantidadIncognitas];
            var breakClico = false;
            var iterador = 0;
            var errorCiclo = 0.0;
            fila = 0;
            double[] solucion = new double[parametros.CantidadIncognitas];

            //Pone el vector solucion en 0
            for (int i = 0; i < parametros.CantidadIncognitas; i++)
            {
                solucion[i] = 0;
            }

            //Verifica el corte
            while (breakClico == false)
            {
                iterador = iterador + 1;
                fila = fila + 1;
                solucion = ObtenerSolucion(solucion, parametros.CantidadIncognitas, vectorIncognitas);

                if (iterador == 1)
                {
                    for (int i = 0; i < parametros.CantidadIncognitas; i++)
                    {
                        solucionAnterior[i] = 0;
                    }
                }

                var resultadoCiclo = VerificarBreakCiclo(parametros.CantidadIncognitas, solucion, solucionAnterior, parametros.Tolerancia, parametros.Iteraciones, iterador, parametros.Tolerancia);
                breakClico = resultadoCiclo.CorteCiclo;
                errorCiclo = resultadoCiclo.ErrorRelativo;
                for (int i = 0; i < parametros.CantidadIncognitas; i++)
                {
                    solucionAnterior[i] = solucion[i];
                }

            }

            return new ResultadoGaussSeidel { Resultados = solucion, ErrorRelativo = errorCiclo, Iteraciones = iterador };

        }

        private ResultadoCiclo VerificarBreakCiclo(int cantidadIncognitas, double[] solucion, double[] solucionAnterior, double tolerancia, int iteracionesPermitidas, int iteracionesCiclo, double errorPermitido)
        {
            bool corta = true;
            var errorRelativo = 0.0;

            for (int i = 0; i < cantidadIncognitas; i++)
            {
                errorRelativo = errorRelativo + Math.Abs((solucion[i] - solucionAnterior[i]) / solucion[i]);

                if (solucion[i] != solucionAnterior[i] || errorRelativo > tolerancia)
                {
                    corta = false;
                }
            }

            if (iteracionesCiclo >= iteracionesPermitidas || Math.Abs(errorRelativo) < errorPermitido)
            {
                corta = true;
            }
            return new ResultadoCiclo(corta, errorRelativo);

        }

        private double CalcularResultado(string funcion, double[] soluciones, int cantidad, int x)
        {

            string valoresFuncion = "F(";
            var cultura = CultureInfo.CreateSpecificCulture("en-US");

            var coloco = 0;
            var incognitas = cantidad - 1;
            for (int i = 0; i < cantidad; i++)
            {
                if (i != x)
                {
                    coloco = coloco + 1;
                    valoresFuncion = valoresFuncion + soluciones[i].ToString(cultura);

                    if (coloco == incognitas)
                    {
                        valoresFuncion = valoresFuncion + ")";
                    }
                    else
                    {
                        valoresFuncion = valoresFuncion + ",";
                    }
                }
            }
            Function function = new Function(funcion);

            Expression expression = new Expression(valoresFuncion, function);
            var calculo = expression.calculate();
            return expression.calculate();
        }

        private double[] ObtenerSolucion(double[] solucion, int cantidadIncognitas, string[] vectorIncognitas)
        {
            var nombreFuncion = "F(";
            var fila = 0;
            while (fila < cantidadIncognitas)
            {
                var coloco = 0;
                var incognitas = cantidadIncognitas - 1;

                for (int i = 0; i < cantidadIncognitas; i++)
                {

                    if (i != fila)
                    {
                        coloco = coloco + 1;

                        nombreFuncion = nombreFuncion + "X" + (i + 1);

                        if (coloco == incognitas)
                        {
                            nombreFuncion = nombreFuncion + ")";
                        }
                        else
                        {
                            nombreFuncion = nombreFuncion + ",";
                        }
                    }
                }

                nombreFuncion = nombreFuncion + " = " + vectorIncognitas[fila];
                solucion[fila] = CalcularResultado(nombreFuncion, solucion, cantidadIncognitas, fila);
                fila = fila + 1;
                nombreFuncion = "F(";

            }

            return solucion;
        }

        private class ResultadoCiclo
        {
            public bool CorteCiclo { get; set; }
            public double ErrorRelativo { get; set; }

            public ResultadoCiclo(bool corte, double error)
            {
                this.CorteCiclo = corte;
                this.ErrorRelativo = error;
            }
        }

        #endregion


        #region GaussJordan
        public ResultadoGaussJordan MetodoGaussJordan(ParametrosGaussJordan parametros)
        {
            double[,] matriz = parametros.Matriz;
            var cantidadFilas = parametros.CantidadIncognitas - 1;
            var cantidadColumnas = parametros.CantidadIncognitas + 1;

            for (int FilasARecorrer = 0; FilasARecorrer < parametros.CantidadIncognitas; FilasARecorrer++)
            {
                matriz = PivoteoParcial(matriz, FilasARecorrer, FilasARecorrer, cantidadFilas, cantidadColumnas);
                double[] vectorFila = VectorFilaNormalizada(matriz, FilasARecorrer, (parametros.CantidadIncognitas + 1));
                var fila = 1;

                if (fila > cantidadFilas)
                {
                    fila = 0;
                }

                var columna = FilasARecorrer;
                var corte = 0;

                while (corte < cantidadFilas)
                {
                    var aEliminar = matriz[fila, columna];
                    double[] vectorFilaMultiplicada = new double[parametros.CantidadIncognitas + 1];
                    if (aEliminar != 0)
                    {
                        for (int j = 0; j <= parametros.CantidadIncognitas; j++)
                        {
                            vectorFilaMultiplicada[j] = vectorFila[j] * aEliminar;
                        }

                        for (int j = 0; j <= parametros.CantidadIncognitas; j++)
                        {
                            matriz[fila, j] = matriz[fila, j] - vectorFilaMultiplicada[j];
                        }
                    }
                    corte = corte + 1;
                    fila = fila + 1;

                    if (fila > cantidadFilas)
                        fila = 0;
                }

                for (int i = 0; i <= parametros.CantidadIncognitas; i++)
                {
                    matriz[0, i] = vectorFila[i];
                }
            }

            double[] resultados = ReturnResultadoGaussJordan(parametros.CantidadIncognitas, matriz);
            return new ResultadoGaussJordan { Resultados = resultados };
        }

        private double[] VectorFilaNormalizada(double[,] matrizSinNormalizar, int columna, int cantidadColumnas)
        {

            var pivot = matrizSinNormalizar[0, columna];
            double[] vectorFilaNormalizada = new double[cantidadColumnas];

            for (int j = 0; j < cantidadColumnas; j++)
            {
                vectorFilaNormalizada[j] = matrizSinNormalizar[0, j] / pivot;
            }

            return vectorFilaNormalizada;
        }
        private double[,] PivoteoParcial(double[,] matriz, int fila, int columna, int cantidadFilas, int cantidadColumnas)
        {
            var valorMax = 0.0;
            var valorAhora = 0.0;
            var filamax = 0;
            var filaOriginal = fila;

            for (int i = fila; i <= cantidadFilas; i++)
            {
                valorAhora = Math.Abs(matriz[fila, columna]);

                if (valorAhora > valorMax)
                {
                    valorMax = valorAhora;
                    filamax = fila;
                }

                fila = fila + 1;
            }


            double[] aPivotear = new double[cantidadColumnas];

            for (int i = 0; i < cantidadColumnas; i++)
            {
                aPivotear[i] = matriz[filamax, i];
            }

            var filaAnterior = 0;
            var filaRemplazo = filamax;
            fila = filaOriginal;

            //Muevo todo para abajo. Ej: si tengo que intercambia la fila 1 con la 3.
            //guardo fila 3, bajo la fila 2 a la fila 3, bajo fila 1 a fila 2 y por ultimo la fila 0= lo guardado de la fila 3. 
            while (filaRemplazo > 0)
            {
                filaAnterior = filaRemplazo - 1;
                for (int j = 0; j < cantidadColumnas; j++)
                {
                    matriz[filaRemplazo, j] = matriz[filaAnterior, j];
                }
                filaRemplazo = filaRemplazo - 1;
            }

            for (int i = 0; i < cantidadColumnas; i++)
            {
                matriz[0, i] = aPivotear[i];
            }

            return matriz;
        }

        private double[] ReturnResultadoGaussJordan(int cantidadIncognitas, double[,] matriz)
        {
            CultureInfo culture = new CultureInfo("en-Us");

            var columna = 0;
            var numero = "";
            double[] resultados = new double[cantidadIncognitas];

            while (columna < cantidadIncognitas)
            {

                for (int i = 0; i < cantidadIncognitas; i++)
                {
                    if (matriz[i, columna] != 0)
                    {
                        numero = matriz[i, cantidadIncognitas].ToString("N7");
                        resultados[columna] = Convert.ToDouble(numero);
                        break;
                    }
                }
                columna = columna + 1;
            }

            return resultados;
        }
        #endregion

    }
}
