﻿using AnalisisNumerico.Entidades.IntegracionNumerica;
using org.mariuszgromada.math.mxparser;

namespace AnalisisNumerico.Logica
{
    public class MetodoIntegracionNumerica : IMetodoIntegracionNumerica
    {
        private readonly IUtilityFunctions utilityFunctions;

        public MetodoIntegracionNumerica(IUtilityFunctions utility)
        {
            utilityFunctions = utility;
        }


        public double MetodoTrapecio(ParametrosMetodosSimples parametros)
        {
            var funcion = new Function(parametros.Funcion);
            var argumentoA = new Argument("x", parametros.PuntoInicial);
            var argumentoB = new Argument("x", parametros.PuntoFinal);
            var nombreFuncion = parametros.Funcion.Split('=')[0].Trim();

            var evaluacionA = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, argumentoA);
            var evaluacionB = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, argumentoB);

            var resultado = ((evaluacionB + evaluacionA) * (parametros.PuntoFinal - parametros.PuntoInicial)) / 2;
            return resultado;

        }

        public double MetodoTrapecioMultiple(ParametrosMetodosMultiples parametros)
        {
            var funcion = new Function(parametros.Funcion);
            var nombreFuncion = parametros.Funcion.Split('=')[0].Trim();

            var h = (parametros.PuntoFinal - parametros.PuntoInicial) / parametros.CantidadIntervalos;
            var sum = 0.0;

            var xi = parametros.PuntoInicial;


            for (int i = 1; i < (parametros.CantidadIntervalos); i++)
            {
                xi = xi + h;
                var argumentoXi = new Argument("x", xi);
                sum = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, argumentoXi) + sum;
            }
            sum = sum * 2;


            var evaluacionX0 = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", parametros.PuntoInicial));
            var evalucionXn = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", parametros.PuntoFinal));

            var resultado = (h / 2) * (evaluacionX0 + sum + evalucionXn);
            return resultado;
        }

        public double MetodoSimpson1_3Simple(ParametrosMetodosSimples parametros)
        {
            var funcion = new Function(parametros.Funcion);
            var nombreFuncion = parametros.Funcion.Split('=')[0].Trim();

            var h = (parametros.PuntoFinal - parametros.PuntoInicial) / 2;


            var evaluacionX0 = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", parametros.PuntoInicial));
            var evaluacionX1 = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", (parametros.PuntoFinal + parametros.PuntoInicial) / 2));
            var evaluacionX2 = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", parametros.PuntoFinal));

            var resultado = (h / 3) * (evaluacionX0 + 4 * evaluacionX1 + evaluacionX2);
            return resultado;
        }

        public double MetodoSimpson(ParametrosMetodosMultiples parametros)
        {
            if (parametros.CantidadIntervalos % 2 == 0)
                return MetodoSimpson1_3Multiple(parametros);
            else
            {
                var h = (parametros.PuntoFinal - parametros.PuntoInicial) / parametros.CantidadIntervalos;

                var parametrosSimpson1_3 = new ParametrosMetodosMultiples { Funcion = parametros.Funcion, PuntoInicial = parametros.PuntoInicial, PuntoFinal = parametros.PuntoFinal - h * 3, CantidadIntervalos = parametros.CantidadIntervalos - 3 };
                var parametrosSimpson3_8 = new ParametrosMetodosSimples { Funcion = parametros.Funcion, PuntoInicial = parametrosSimpson1_3.PuntoFinal, PuntoFinal = parametros.PuntoFinal };

                var resultadoSimpson1_3 = MetodoSimpson1_3Multiple(parametrosSimpson1_3);
                var resultadoSimpson3_8 = MetodoSimpson3_8Simple(parametrosSimpson3_8);

                return resultadoSimpson1_3 + resultadoSimpson3_8;

            }

        }

        public double MetodoSimpson1_3Multiple(ParametrosMetodosMultiples parametros)
        {
            var funcion = new Function(parametros.Funcion);
            var nombreFuncion = parametros.Funcion.Split('=')[0].Trim();

            var h = (parametros.PuntoFinal - parametros.PuntoInicial) / parametros.CantidadIntervalos;

            var sum1 = 0.0;
            var xiInicial = parametros.PuntoInicial;
            var xi = xiInicial;
            var hAUsar = h * 2;

            for (int i = 1; i < parametros.CantidadIntervalos; i = i + 2)
            {
                if (i == 1)
                    xi = parametros.PuntoInicial + h;
                else
                    xi = xi + hAUsar;
                var argumentoXi = new Argument("x", xi);
                sum1 = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, argumentoXi) + sum1;
            }
            sum1 = 4 * sum1;

            var sum2 = 0.0;
            xi = xiInicial;

            for (int i = 2; i < parametros.CantidadIntervalos - 1; i = i + 2)
            {
                xi = xi + hAUsar;
                var argumentoXi = new Argument("x", xi);
                sum2 = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, argumentoXi) + sum2;
            }
            sum2 = 2 * sum2;
            var evaluacionX0 = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", parametros.PuntoInicial));
            var evaluacionXn = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", parametros.PuntoFinal));

            var resultado = (h / 3) * (evaluacionX0 + sum1 + sum2 + evaluacionXn);
            return resultado;
        }

        private double MetodoSimpson3_8Simple(ParametrosMetodosSimples parametros)
        {
            var funcion = new Function(parametros.Funcion);
            var nombreFuncion = parametros.Funcion.Split('=')[0].Trim();

            var h = (parametros.PuntoFinal - parametros.PuntoInicial) / 3;

            var evaluacionX0 = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", parametros.PuntoInicial));
            var evaluacionX1 = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", parametros.PuntoInicial + h));
            var evalucionX2 = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", parametros.PuntoFinal - h));
            var evaluacionX3 = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", parametros.PuntoFinal));

            var resultado = (0.375 * h) * (evaluacionX0 + 3 * evaluacionX1 + 3 * evalucionX2 + evaluacionX3);
            return resultado;
        }


    }
}