﻿using AnalisisNumerico.Entidades;
using AnalisisNumerico.Entidades.Raices;
using org.mariuszgromada.math.mxparser;
using System;

namespace AnalisisNumerico.Logica
{
    public class MetodoRaices : IMetodosRaices
    {

        private readonly IUtilityFunctions utilityFunctions;
        public MetodoRaices(IUtilityFunctions utility)
        {
            this.utilityFunctions = utility;
        }

        private delegate double MetodoCerradoDelegate(double xi, double xd, string nombre, Function function);

        public ResultadoRaices MetodoBiseccion(ParametrosBiseccion parametros)
        {
            return MetodoCalculosRaicesCerrados(parametros, CalcularXrBiseccion);
        }

        public ResultadoRaices MetodoReglaFalsa(ParametrosBiseccion parametros)
        {
            return MetodoCalculosRaicesCerrados(parametros, CalcularXrReglaFalsa);
        }

        public ResultadoRaices MetodoTangente(ParametrosTangente parametros)
        {
            var funcion = new Function(parametros.Funcion);
            var argumentoXi = new Argument("x", parametros.Xi);
            var nombreFuncion = parametros.Funcion.Split('=')[0].Trim();
            var evaluacionXi = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, argumentoXi);
            var resultado = 0.0;
            var iterador = 0;
            var xAnterior = 0.0;
            var xr = 0.0;
            var errorRelativoString = "";
            var xi = parametros.Xi;
            var evaluarXr = 0.0;
            var resultadoString = "";
            var derivada = 0.0;

            if (evaluacionXi == 0)
            {
                xr = parametros.Xi;
                errorRelativoString = "0";
            }
            else
            {
                do
                {
                    derivada = CalcularDerivada(xi, nombreFuncion, funcion, parametros.Tolerancia);
                    iterador = iterador + 1;

                    var evaluarXi = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", xi));

                    xr = xi - (evaluarXi / derivada);
                    evaluarXr = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", xr));

                    var corteCiclo = VerificarBreakCiclo(xr, parametros.Tolerancia, evaluarXr, xAnterior);
                    errorRelativoString = corteCiclo.ErrorRelativo;

                    if (corteCiclo.CorteCiclo)
                        break;

                    xAnterior = xr;
                    xi = xr;

                } while (iterador < parametros.Iteraciones);

            }

            resultado = xr;
            resultadoString = ConvertirADecimal(resultado);
            return new ResultadoRaices
            {
                Raiz = resultadoString,
                Iteraciones = iterador,
                ErrorRelativo = errorRelativoString,
            };

        }

        public ResultadoRaices MetodoSecante(ParametrosSecante parametros)
        {
            var funcion = new Function(parametros.Funcion);
            var argumentoXi = new Argument("x", parametros.Xi);
            var argumentoXd = new Argument("x", parametros.Xd);
            var nombreFuncion = parametros.Funcion.Split('=')[0].Trim();
            var evaluacionXi = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, argumentoXi);
            var evalucionXd = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, argumentoXd);
            var resultado = 0.0;
            var iterador = 0;
            var xAnterior = 0.0;
            var xr = 0.0;
            var errorRelativoString = "";
            var xi = parametros.Xi;
            var xd = parametros.Xd;
            var evaluarXr = 0.0;

            if (evaluacionXi == 0)
            {
                xr = xi;
                errorRelativoString = "0";
            }
            else
            {
                if (evalucionXd == 0)
                {
                    xr = xd;
                    errorRelativoString = "0";
                }
                else
                {
                    do
                    {
                        iterador = iterador + 1;
                        var evaluarXd = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", xd));
                        var evaluarXi = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", xi));

                        xr = (evaluarXd * xi - evaluarXi * xd) / (evaluarXd - evaluarXi);
                        evaluarXr = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", xr));

                        var corteCiclo = VerificarBreakCiclo(xr, parametros.Tolerancia, evaluarXr, xAnterior);
                        errorRelativoString = corteCiclo.ErrorRelativo;

                        if (corteCiclo.CorteCiclo)
                            break;

                        xd = xi;
                        xi = xr;
                        xAnterior = xr;

                    } while (iterador < parametros.Iteraciones);
                }

            }

            resultado = xr;

            return new ResultadoRaices
            {
                Raiz = ConvertirADecimal(resultado),
                Iteraciones = iterador,
                ErrorRelativo = errorRelativoString,
            };
        }

        public double CalcularDerivada(double xi, string nombreFuncion, Function funcion, double tolerancia)
        {
            //Es publico por el test.
            var xiTolerancia = xi + tolerancia;
            var evaluacionXiTolerancia = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", xiTolerancia));
            var evaluacionXi = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", xi));

            return (evaluacionXiTolerancia - evaluacionXi) / tolerancia;

        }

        public string ConvertirADecimal(double numero)
        {
            var convertido = numero.ToString("N7");
            return convertido;
        }

        public double CalcularErrorRelativo(double xr, double xrAnterior)
        {
            var error = (xr - xrAnterior) / xr;
            return Math.Abs(error);
        }

        private ResultadoRaices MetodoCalculosRaicesCerrados(ParametrosBiseccion parametros, MetodoCerradoDelegate calcularXr)
        {
            var funcion = new Function(parametros.Funcion);
            var argumentoXi = new Argument("x", parametros.Xi);
            var argumentoXd = new Argument("x", parametros.Xd);
            var nombreFuncion = parametros.Funcion.Split('=')[0].Trim();
            var evaluacionXi = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, argumentoXi);
            var evalucionXd = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, argumentoXd);
            var resultadoProducto = (evaluacionXi) * (evalucionXd);
            var resultado = 0.0;
            var resultadoString = "";
            if (resultadoProducto > 0)
            {
                var exception = new ArgumentException("Vuelva a ingresar el intervalo, la raiz no se encuentra en el mismo");
                throw exception;
            }
            else
            {
                var iterador = 0;
                var xAnterior = 0.0;
                var xr = 0.0;
                var errorRelativoString = "";
                var xi = parametros.Xi;
                var xd = parametros.Xd;
                var evaluarXr = 0.0;

                if (resultadoProducto == 0)
                {
                    if (evaluacionXi == 0)
                        xr = parametros.Xi;
                    else
                        xr = parametros.Xd;

                    errorRelativoString = "0";
                }
                else
                {
                    do
                    {
                        iterador = iterador + 1;

                        xr = calcularXr(xi, xd, nombreFuncion, funcion);
                        evaluarXr = utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", xr));

                        var corteClico = VerificarBreakCiclo(xr, parametros.Tolerancia, evaluarXr, xAnterior);
                        errorRelativoString = corteClico.ErrorRelativo;

                        if (corteClico.CorteCiclo)
                            break;

                        var producto = evaluarXr * utilityFunctions.EvaluarFuncion(nombreFuncion, funcion, new Argument("x", xi));

                        if (producto > 0)
                            xi = xr;
                        else
                            xd = xr;

                        xAnterior = xr;
                    } while (iterador < parametros.Iteraciones);
                }

                resultado = xr;
                resultadoString = ConvertirADecimal(resultado);

                return new ResultadoRaices
                {
                    Raiz = resultadoString,
                    Iteraciones = iterador,
                    ErrorRelativo = errorRelativoString,
                };
            }
        }

        private double CalcularXrBiseccion(double xi, double xd, string nombre, Function function)
        {
            return (xi + xd) / 2;
        }

        private double CalcularXrReglaFalsa(double xi, double xd, string nombre, Function function)
        {
            var argumentoXi = new Argument("x", xi);
            var argumentoXd = new Argument("x", xd);

            var evaluarXi = utilityFunctions.EvaluarFuncion(nombre, function, argumentoXi);
            var evaluarXd = utilityFunctions.EvaluarFuncion(nombre, function, argumentoXd);

            return ((evaluarXd * xi) - (evaluarXi * xd)) / (evaluarXd - evaluarXi);
        }

        private ResultadoCiclo VerificarBreakCiclo(double xr, double tolerancia, double evaluacionXr, double xAnterior)
        {
            var mensajeError = "No se puede calcular el error relativo";
            var errorRelativoString = "";
            var errorRelativo = 0.0;
            var corte = false;

            if (xr == 0 || Math.Abs(xr) < tolerancia)
            {
                errorRelativoString = mensajeError;

                if (evaluacionXr == 0 || Math.Abs(evaluacionXr) < tolerancia)
                    corte = true;
                else
                    corte = false;
            }
            else
            {
                errorRelativo = CalcularErrorRelativo(xr, xAnterior);
                errorRelativoString = ConvertirADecimal(errorRelativo);
            }

            if (evaluacionXr == 0 || Math.Abs(evaluacionXr) <= tolerancia)
                corte = true;
            else
            {
                if (errorRelativoString != mensajeError)
                {
                    if (Math.Abs(errorRelativo) < tolerancia)
                        corte = true;
                }
            }

            if (corte)
                return new ResultadoCiclo(true, errorRelativoString);
            else
                return new ResultadoCiclo(false, errorRelativoString);

        }

        private class ResultadoCiclo
        {
            public bool CorteCiclo { get; set; }
            public string ErrorRelativo { get; set; }

            public ResultadoCiclo(bool corte, string error)
            {
                this.CorteCiclo = corte;
                this.ErrorRelativo = error;
            }
        }
    }
}