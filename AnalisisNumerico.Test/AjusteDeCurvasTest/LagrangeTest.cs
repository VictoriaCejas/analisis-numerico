﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AnalisisNumerico.Logica;
using AnalisisNumerico.Entidades.AjustesDeCurvas;

namespace AnalisisNumerico.Test.AjusteDeCurvasTest
{
    [TestClass]
    public class LagrangeTest
    {
        
        [TestMethod]
        public void TestMethod1()
        {
            
            double[,] matriz = new double[4, 2];
            matriz[0, 0] = 1 ;
            matriz[0, 1] = 56.5;
            matriz[1, 0] = 5;
            matriz[1, 1] = 113.0;
            matriz[2, 0] = 20;
            matriz[2, 1] = 181.0;
            matriz[3, 0] = 40;
            matriz[3, 1] = 214.5;

            var metodoEcuaciones = new MetodoEcuaciones();

            var metodo = new MetodoAjusteDeCurvas(metodoEcuaciones);

            var resultadometodo = metodo.MetodoLagrange(new ParametrosLagrange() { Tabla= matriz, X=2});
            Assert.AreEqual(72.725384615384613, resultadometodo.Resultado);
          
        }
    }
}
