﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnalisisNumerico.Entidades.AjustesDeCurvas;
using AnalisisNumerico.Entidades.Ecuaciones;
using AnalisisNumerico.Logica;

namespace AnalisisNumerico.Test.AjusteDeCurvasTest
{
    [TestClass]
    public class RegresionLinealTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            double[,] matriz = new double[5, 2];
            matriz[0, 0] = -3;
            matriz[0, 1] = -7.5;
            matriz[1, 0] = 0.5;
            matriz[1, 1] = -2.25;
            matriz[2, 0] = 1;
            matriz[2, 1] = -1.5;
            matriz[3, 0] = 2;
            matriz[3, 1] = 0;
            matriz[4, 0] = 5;
            matriz[4, 1] = 4.5;

            var metodoEcuaciones = new MetodoEcuaciones();

            var metodo = new MetodoAjusteDeCurvas(metodoEcuaciones);

            var resultado = metodo.MetodoRegresionLineal(new ParametrosRegresionLineal { Tabla = matriz, CantidadPuntos = 5 });
            double[] resultadoEsperado = new double[] { -3, 1.5};
            double[] resultadoDevuelto = resultado.valores;

            for (int i = 0; i < resultado.valores.Length; i++)
            {
                Assert.AreEqual(resultadoEsperado[i], resultado.valores[i]);
            }
        }
    }
}
