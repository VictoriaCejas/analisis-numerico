﻿using AnalisisNumerico.Entidades.AjustesDeCurvas;
using AnalisisNumerico.Logica;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AnalisisNumerico.Test.AjusteDeCurvasTest
{
    [TestClass]
    public class RegresionPolinomialTest
    {


        [TestMethod]
        public void DeberiaDarBien()
        {

            double[,] matriz = new double[6, 2];
            matriz[0, 0] = 0;
            matriz[0, 1] = 2.1;
            matriz[1, 0] = 1;
            matriz[1, 1] = 7.7;
            matriz[2, 0] = 2;
            matriz[2, 1] = 13.6;
            matriz[3, 0] = 3;
            matriz[3, 1] = 27.2;
            matriz[4, 0] = 4;
            matriz[4, 1] = 40.9;
            matriz[5, 0] = 5;
            matriz[5, 1] = 61.1;

            var metodoEcuaciones = new MetodoEcuaciones();

            var metodo = new MetodoAjusteDeCurvas(metodoEcuaciones);

            var resultadoEsperado = new double[] { 2.2150794, 3.994709, 0.6513889, 0.2842593, -0.0208333 };
            var resultadometodo = metodo.MetodoRegresionPolinomial(new ParametrosRegresionPolinomial { Tabla = matriz, Grado = 4 });

            for (int i = 0; i < resultadometodo.valores.Length; i++)
            {
                Assert.AreEqual(resultadoEsperado[i], resultadometodo.valores[i]);
            }
        }
    }
}
