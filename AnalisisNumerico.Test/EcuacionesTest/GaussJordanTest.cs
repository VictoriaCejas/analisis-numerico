﻿using AnalisisNumerico.Entidades.Ecuaciones;
using AnalisisNumerico.Logica;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AnalisisNumerico.Test.EcuacionesTest
{

    [TestClass]
    public class GaussJordanTest
    {
        MetodoEcuaciones metodo = new MetodoEcuaciones();

        [TestMethod]
        public void DeberiaDarBienElResultado()
        {
            double[,] matriz = new double[3, 4];
            matriz[0, 0] = 2;
            matriz[0, 1] = -6;
            matriz[0, 2] = 12;
            matriz[0, 3] = 20;
            matriz[1, 0] = 3;
            matriz[1, 1] = 5;
            matriz[1, 2] = -1;
            matriz[1, 3] = 8;
            matriz[2, 0] = 6;
            matriz[2, 1] = -8;
            matriz[2, 2] = 2;
            matriz[2, 3] = 6;

            ResultadoGaussJordan resultado = metodo.MetodoGaussJordan(new ParametrosGaussJordan
            {
                Matriz = matriz,
                CantidadIncognitas = 3,
            });

            var esperado = new double[] { 1.6713287, 0.972028, 1.8741259 };
            for (int i = 0; i < resultado.Resultados.Length; i++)
            {
                Assert.AreEqual(esperado[i], resultado.Resultados[i]);
            }


        }
    }
}
