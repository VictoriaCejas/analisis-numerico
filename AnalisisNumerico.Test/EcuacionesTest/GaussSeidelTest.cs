﻿using AnalisisNumerico.Entidades.Ecuaciones;
using AnalisisNumerico.Logica;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AnalisisNumerico.Test.EcuacionesTest
{
    [TestClass]
    public class GaussSeidelTest
    {
        MetodoEcuaciones metodo = new MetodoEcuaciones();

        [TestMethod]
        public void DeberiaDarBienLasIncognitas()
        {
            double[,] matriz = new double[4, 5];
            matriz[0, 0] = 4;
            matriz[0, 1] = 1;
            matriz[0, 2] = 1;
            matriz[0, 3] = 1;
            matriz[0, 4] = 3;
            matriz[1, 0] = 0;
            matriz[1, 1] = 4;
            matriz[1, 2] = 1;
            matriz[1, 3] = 1;
            matriz[1, 4] = 5;
            matriz[2, 0] = 1;
            matriz[2, 1] = 1;
            matriz[2, 2] = 3;
            matriz[2, 3] = 0;
            matriz[2, 4] = 0;
            matriz[3, 0] = 1;
            matriz[3, 1] = 1;
            matriz[3, 2] = 0;
            matriz[3, 3] = 4;
            matriz[3, 4] = -5;

            var resultadoEsperado = new double[] { 0.999993792831708, 2.0000013087403, -0.99999836719067, -1.999998775393 };


            ResultadoGaussSeidel resultado = metodo.MetodoGaussSeidel(new ParametrosGaussSeidel { Matriz = matriz, CantidadIncognitas = 4, Iteraciones = 100, Tolerancia = 0.0001 });

            for (int i = 0; i < resultado.Resultados.Length; i++)
            {
                Assert.AreEqual(resultadoEsperado[i], resultado.Resultados[i]);
            }

            Assert.AreEqual(3.5618082134238453E-05, resultado.ErrorRelativo);
            Assert.AreEqual(8, resultado.Iteraciones);


        }
    }
}
