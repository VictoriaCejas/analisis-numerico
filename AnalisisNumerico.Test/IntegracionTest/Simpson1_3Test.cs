﻿using AnalisisNumerico.Entidades.IntegracionNumerica;
using AnalisisNumerico.Logica;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AnalisisNumerico.Test.IntegracionTest
{
    [TestClass]
    public class Simpson1_3Test
    {
        [TestMethod]
        public void DeberiaDarBienSimpson1_3Simple()
        {
            var funcion = "f(x)=(1/(x+0.5))+(1/4*x^2)";
            var metodo = new MetodoIntegracionNumerica(new UtilityFunctions());
            var resultado = metodo.MetodoSimpson1_3Simple(new ParametrosMetodosSimples { Funcion = funcion, PuntoInicial = 0, PuntoFinal = 3 });

            Assert.AreEqual("4,39285714285715", resultado.ToString());
        }

    }
}
