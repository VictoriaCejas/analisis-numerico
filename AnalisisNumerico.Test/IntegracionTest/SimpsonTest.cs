﻿using System;
using AnalisisNumerico.Entidades.IntegracionNumerica;
using AnalisisNumerico.Logica;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AnalisisNumerico.Test.IntegracionTest
{
    [TestClass]
    public class SimpsonTest
    {
        MetodoIntegracionNumerica metodo = new MetodoIntegracionNumerica(new UtilityFunctions());


        [TestMethod]
        public void DeberiaDarBienSimpson1_3Multiple()
        {
            var funcion = "f(x)=(1/(x+0.5))+(1/4*x^2)";

            var resultado = metodo.MetodoSimpson(new ParametrosMetodosMultiples { Funcion = funcion, PuntoInicial = 0, PuntoFinal = 3, CantidadIntervalos = 10 });

            Assert.AreEqual("4,19851914775409", resultado.ToString());
        }

        [TestMethod]
        public void DeberiaDarBienSimpson()
        {
            var funcion = "f(x)=(1/(x+0.5))+(1/4*x^2)";

            var resultado = metodo.MetodoSimpson(new ParametrosMetodosMultiples { Funcion = funcion, PuntoInicial = 0, PuntoFinal = 3, CantidadIntervalos = 25 });

            Assert.AreEqual("4,19600836540774", resultado.ToString());
        }
    }
}
