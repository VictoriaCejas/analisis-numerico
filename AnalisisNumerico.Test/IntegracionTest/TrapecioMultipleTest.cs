﻿using AnalisisNumerico.Entidades.IntegracionNumerica;
using AnalisisNumerico.Logica;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AnalisisNumerico.Test.IntegracionTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void DeberiaDarBienTrapecioMultiple()
        {
            var funcion = "f(x)=(1/(x+0.5))+(1/4*x^2)";
            var metodo = new MetodoIntegracionNumerica(new UtilityFunctions());
            var resultado = metodo.MetodoTrapecioMultiple(new ParametrosMetodosMultiples { Funcion = funcion, PuntoInicial = 0, PuntoFinal = 3, CantidadIntervalos = 20 });

            Assert.AreEqual("4,20600477169488", resultado.ToString());
        }
    }
}
