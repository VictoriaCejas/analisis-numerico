﻿using AnalisisNumerico.Entidades.IntegracionNumerica;
using AnalisisNumerico.Logica;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AnalisisNumerico.Test.IntegracionTest
{
    [TestClass]
    public class TrapecioSimpleTest
    {

        [TestMethod]
        public void DeberiaDarBienTrapecioSimple()
        {
            var funcion = "f(x)=(1/(x+0.5))+(1/4*x^2)";
            var metodo = new MetodoIntegracionNumerica(new UtilityFunctions());
            var resultado = metodo.MetodoTrapecio(new ParametrosMetodosSimples { Funcion = funcion, PuntoInicial = 0, PuntoFinal = 3 });

            Assert.AreEqual("6,80357142857144", resultado.ToString());
        }


    }
}
