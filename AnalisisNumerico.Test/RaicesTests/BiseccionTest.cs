﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AnalisisNumerico.Logica;
using AnalisisNumerico.Entidades;


namespace AnalisisNumerico.Test.RaicesTests
{
    [TestClass]
    public class BiseccionTest
    {
        MetodoRaices metodo = new MetodoRaices(new UtilityFunctions());

        [TestMethod]
        public void DeberiaHacerBienElCalculoDeRaiz()
        {
            var resultado = metodo.MetodoBiseccion(new ParametrosBiseccion { Funcion = "f(x)=x^3-8", Iteraciones = 100, Tolerancia = 0.0001, Xi = 1, Xd = 17, EsBiseccion = true });
            Assert.AreEqual("2,0000000", resultado.Raiz);
        }

        [TestMethod]
        public void XiDeberiaSerRaiz()
        {
            var resultado = metodo.MetodoBiseccion(new ParametrosBiseccion { Funcion = "f(x)=x ^ 3-8", Iteraciones = 100, Tolerancia = 0.0001, Xi = 2, Xd = 17, EsBiseccion = true });
            Assert.AreEqual("2,0000000", resultado.Raiz);
        }

        [TestMethod]
        public void XdDeberiaSerRaiz()
        {
            var resultado = metodo.MetodoBiseccion(new ParametrosBiseccion { Funcion = "f(x)=x ^ 3-8", Iteraciones = 100, Tolerancia = 0.0001, Xi = -5, Xd = 2, EsBiseccion = true });
            Assert.AreEqual("2,0000000", resultado.Raiz);

        }
        [TestMethod]
        public void DeberiaCortarAntesPorIteraciones()
        {
            var resultado = metodo.MetodoBiseccion(new ParametrosBiseccion { Funcion = "f(x)=x ^ 3-8", Iteraciones = 3, Tolerancia = 0.0001, Xi = 1, Xd = 17, EsBiseccion = true });
            Assert.AreEqual(3, resultado.Iteraciones);
        }

        [TestMethod]
        public void DeberiaCortarAntesPorErrorMenorATolerancia()
        {
            var resultado = metodo.MetodoBiseccion(new ParametrosBiseccion { Funcion = "f(x)= ((x^5-1)*(e^x))-10", Iteraciones = 100, Tolerancia = 0.0001, Xi = 1, Xd = 1.5, EsBiseccion = true });
            Assert.AreEqual("0,0000939", resultado.ErrorRelativo);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void DeberiaLanzarExcepcionPorProductoMayorACero()
        {
            var resultado = metodo.MetodoBiseccion(new ParametrosBiseccion { Funcion = "f(x)=x ^ 3-8", Iteraciones = 100, Tolerancia = 0.0001, Xi = 5, Xd = 8, EsBiseccion = true });
        }
        [TestMethod]
        public void NoDeberiaPoderEvaluarError()
        {
            var resultado = metodo.MetodoBiseccion(new ParametrosBiseccion { Funcion = "f(x)=x^2+4*x", Iteraciones = 100, Tolerancia = 0.0001, Xi = -1, Xd = 2, EsBiseccion = true });
            var mensaje = "No se puede calcular el error relativo";
            Assert.AreEqual(mensaje, resultado.ErrorRelativo);

        }
        [TestMethod]
        public void CambiaElFormato()
        {
            var resultado = metodo.ConvertirADecimal(1.6435896258E-05);
            Assert.AreEqual("0,0000164", resultado);

        }
    }
}
