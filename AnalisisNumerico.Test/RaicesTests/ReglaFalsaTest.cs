﻿using AnalisisNumerico.Entidades;
using AnalisisNumerico.Logica;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AnalisisNumerico.Test.RaicesTests
{
    [TestClass]
    public class ReglaFalsaTest
    {

        MetodoRaices metodo = new MetodoRaices(new UtilityFunctions());

        [TestMethod]
        public void DeberiaHacerBienElCalculoDeLaRaiz()
        {
            var resultado = metodo.MetodoReglaFalsa(new ParametrosBiseccion { Funcion = "f(x)= ((x^5-1)*(e^x))-10", Iteraciones = 100, Tolerancia = 0.0001, Xi = 1, Xd = 1.5, EsBiseccion = false });
            Assert.AreEqual("1,3006826", (resultado.Raiz).ToString());
        }


        public void DeberiaCortarAntesPorIteraciones()
        {
            var resultado = metodo.MetodoReglaFalsa(new ParametrosBiseccion { Funcion = "f(x)= ((x^5-1)*(e^x))-10", Iteraciones = 6, Tolerancia = 0.0001, Xi = 1, Xd = 1.5, EsBiseccion = false });
            Assert.AreEqual(6, resultado.Iteraciones);
        }
    }
}
