﻿using AnalisisNumerico.Entidades.Raices;
using AnalisisNumerico.Logica;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AnalisisNumerico.Test.RaicesTests
{
    [TestClass]
    public class SecanteTest
    {

        MetodoRaices metodo = new MetodoRaices(new UtilityFunctions());

        [TestMethod]
        public void DeberiaHacerBienElCalculoDeRaiz()
        {
            var resultado = metodo.MetodoSecante(new ParametrosSecante { Funcion = "f(x)=x^3-8", Iteraciones = 100, Tolerancia = 0.0001, Xi = 1, Xd = 4 });
            Assert.AreEqual("1,9999989", resultado.Raiz);
        }
        [TestMethod]
        public void XiDeberiaSerRaiz()
        {
            var resultado = metodo.MetodoSecante(new ParametrosSecante { Funcion = "f(x)=x^3-8", Iteraciones = 100, Tolerancia = 0.0001, Xi = 2, Xd = 4 });
            Assert.AreEqual("2,0000000", resultado.Raiz);
        }
        [TestMethod]
        public void XdDeberiaSerRaiz()
        {
            var resultado = metodo.MetodoSecante(new ParametrosSecante { Funcion = "f(x)=x^3-8", Iteraciones = 100, Tolerancia = 0.0001, Xi = 1, Xd = 2 });
            Assert.AreEqual("2,0000000", resultado.Raiz);
        }
        [TestMethod]
        public void DeberiaCortarAntesPorIteraciones()
        {
            var resultado = metodo.MetodoSecante(new ParametrosSecante { Funcion = "f(x)=x^3-8", Iteraciones = 2, Tolerancia = 0.0001, Xi = 1, Xd = 4 });
            Assert.AreEqual(2, resultado.Iteraciones);
        }
        [TestMethod]
        public void DeberiaCortarAntesPorErrorRelativoMenorATolerancia()
        {
            var resultado = metodo.MetodoSecante(new ParametrosSecante { Funcion = "f(x)= ((x^5-1)*(e^x))-10", Iteraciones = 100, Tolerancia = 0.0001, Xi = 1, Xd = 1.5 });
            Assert.AreEqual("0,0000201", resultado.ErrorRelativo);
        }

    }
}
