﻿using AnalisisNumerico.Entidades.Raices;
using AnalisisNumerico.Logica;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using org.mariuszgromada.math.mxparser;

namespace AnalisisNumerico.Test.RaicesTests
{
    [TestClass]
    public class TangenteTest
    {
        public MetodoRaices metodo { get; set; }
        public UtilityFunctions utility { get; set; }
        public TangenteTest()
        {
            this.utility = new UtilityFunctions();
            this.metodo = new MetodoRaices(utility);
        }

        [TestMethod]
        public void DeberiaHacerBienElCalculoDeLaRaiz()
        {

            var resultado = metodo.MetodoTangente(new ParametrosTangente { Funcion = "f(x)=x^3-8", Iteraciones = 100, Tolerancia = 0.0001, Xi = 3 });
            Assert.AreEqual("2,0000002", resultado.Raiz);
        }

        [TestMethod]
        public void DeberiaDerivarBien()
        {
            var funcionDeEntrada = "f(x)=x^3-8";
            var funcion = new Function(funcionDeEntrada);
            var argumentoXi = new Argument("x", 3);
            var nombreFuncion = funcionDeEntrada.Split('=')[0].Trim();
            var evaluacionXi = utility.EvaluarFuncion(nombreFuncion, funcion, argumentoXi);

            var derivada = metodo.CalcularDerivada(3, nombreFuncion, funcion, 0.0001);

            Assert.AreEqual("27,0009000099947", derivada.ToString());
        }

        [TestMethod]
        public void XiDeberiaDarRaiz()
        {
            var resultado = metodo.MetodoTangente(new ParametrosTangente { Funcion = "f(x)=x^3-8", Iteraciones = 100, Tolerancia = 0.0001, Xi = 2 });
            Assert.AreEqual("2,0000000", resultado.Raiz);
            Assert.AreEqual("0", resultado.ErrorRelativo);
            Assert.AreEqual(0, resultado.Iteraciones);
        }
    }

}
