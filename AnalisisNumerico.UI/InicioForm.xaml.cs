﻿using System.Windows;
namespace AnalisisNumerico.UI
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class InicioForm : Window
    {

        public InicioForm()
        {
            InitializeComponent();
        }

        private void BiseccionMenu_Click(object sender, RoutedEventArgs e)
        {
            var biseccionForm = Program.container.GetInstance<MetodosRaicesForm>();
            biseccionForm.Metodo = MetodosRaices.Biseccion;
            biseccionForm.Title = "Biseccion";
            biseccionForm.Show();

        }

        private void ReglaFalsaMenu_Click(object sender, RoutedEventArgs e)
        {
            var reglaFalsaForm = Program.container.GetInstance<MetodosRaicesForm>();
            reglaFalsaForm.Metodo = MetodosRaices.ReglaFalsa;
            reglaFalsaForm.Title = "Regla falsa";
            reglaFalsaForm.Show();
        }

        private void SecanteMenu_Click(object sender, RoutedEventArgs e)
        {
            var secanteForm = Program.container.GetInstance<MetodosRaicesForm>();
            secanteForm.Metodo = MetodosRaices.Secante;
            secanteForm.Title = "Secante";
            secanteForm.Show();
        }

        private void NewtonRapshonMenu_Click(object sender, RoutedEventArgs e)
        {
            var tangenteForm = Program.container.GetInstance<MetodosRaicesForm>();
            tangenteForm.Metodo = MetodosRaices.Tangente;
            tangenteForm.Title = "Newton-Raphson";
            tangenteForm.Show();
        }

        private void GaussJordanMenu_Click(object sender, RoutedEventArgs e)
        {
            var gaussJordanForm = Program.container.GetInstance<MetodoEcuacionesForm>();
            gaussJordanForm.Metodo = MetodosEcuaciones.GaussJordan;
            gaussJordanForm.Title = "Gauss-Jordan";
            gaussJordanForm.Show();
        }

        private void GaussSeidelMenu_Click(object sender, RoutedEventArgs e)
        {
            var gaussSeidelForm = Program.container.GetInstance<MetodoEcuacionesForm>();
            gaussSeidelForm.Metodo = MetodosEcuaciones.GaussSeidel;
            gaussSeidelForm.Title = "Gauss-Seidel";
            gaussSeidelForm.Show();
        }

        private void RegresionLinealMenu_Click(object sender, RoutedEventArgs e)
        {
            var regresionLineal = Program.container.GetInstance<MetodoAjusteDeCurvasForm>();
            regresionLineal.Metodo = MetodoADC.RegresionLineal;
            regresionLineal.Title = "Regresion lineal";
            regresionLineal.Show();
        }

        private void RegresionPolinomialMenu_Click(object sender, RoutedEventArgs e)
        {
            var regresionPolinomial = Program.container.GetInstance<MetodoAjusteDeCurvasForm>();
            regresionPolinomial.Metodo = MetodoADC.RegresionPolinomial;
            regresionPolinomial.Title = "Regresion polinomial";
            regresionPolinomial.Show();
        }

        private void LagrangeMenu_Click(object sender, RoutedEventArgs e)
        {
            var lagrange = Program.container.GetInstance<MetodoAjusteDeCurvasForm>();
            lagrange.Metodo = MetodoADC.Lagrange;
            lagrange.Title = "Regresion polinomial de Lagrange";
            lagrange.Show();
        }

        private void TrapecioMenu_Click(object sender, RoutedEventArgs e)
        {
            var trapecioSimple = Program.container.GetInstance<MetodoDeIntegracionForm>();
            trapecioSimple.Metodo = MetodoIN.Trapecio;
            trapecioSimple.Title = "Trapecio simple";
            trapecioSimple.Show();
        }

        private void TrapecionCMenu_Click(object sender, RoutedEventArgs e)
        {
            var trapecioCompuesto = Program.container.GetInstance<MetodoDeIntegracionForm>();
            trapecioCompuesto.Metodo = MetodoIN.TrapecioMultiple;
            trapecioCompuesto.Title = "Trapecio multiple";
            trapecioCompuesto.Show();
        }

        private void Simpson13Menu_Click(object sender, RoutedEventArgs e)
        {
            var Simpson1_3 = Program.container.GetInstance<MetodoDeIntegracionForm>();
            Simpson1_3.Metodo = MetodoIN.Simpson1_3;
            Simpson1_3.Title = "Simpson 1/3";
            Simpson1_3.Show();

        }

        private void Simpson38Menu_Click(object sender, RoutedEventArgs e)
        {
            var Simpson1_3M = Program.container.GetInstance<MetodoDeIntegracionForm>();
            Simpson1_3M.Metodo = MetodoIN.Simpson1_3Multiple;
            Simpson1_3M.Title = "Simpson 1/3 Multiple";
            Simpson1_3M.Show();

        }
    }
}
