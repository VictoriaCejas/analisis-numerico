﻿using AnalisisNumerico.Entidades.AjustesDeCurvas;
using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace AnalisisNumerico.UI
{
    /// <summary>
    /// Lógica de interacción para MetodoAjusteDeCurvasForm.xaml
    /// </summary>
    public enum MetodoADC
    {
        RegresionLineal,
        RegresionPolinomial,
        Lagrange,
    }
    public partial class MetodoAjusteDeCurvasForm : Window
    {
        public MetodoADC Metodo;
        private readonly IMetodoAjusteDeCurvas metodoAjusteDeCurvas;

        public MetodoAjusteDeCurvasForm(IMetodoAjusteDeCurvas metodoAjusteDeCurvas)
        {
            this.metodoAjusteDeCurvas = metodoAjusteDeCurvas;
            InitializeComponent();
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            if (Metodo != MetodoADC.RegresionLineal)
            {
                A0Txt.Visibility = Visibility.Hidden;
                A0Lbl.Visibility = Visibility.Hidden;
                A1Lbl.Visibility = Visibility.Hidden;
                A1Txt.Visibility = Visibility.Hidden;
                CorrelacionTxt.Visibility = Visibility.Hidden;
                Correlacionlbl.Visibility = Visibility.Hidden;
            }
            else
            {
                Gradolbl.Visibility = Visibility.Hidden;
                GradoTxt.Visibility = Visibility.Hidden;
            }

            if (Metodo == MetodoADC.Lagrange)
            {
                Gradolbl.Content = "X a evaluar";
                PolinomioLbl.Visibility = Visibility.Visible;
                PolinomioTxt.Visibility = Visibility.Visible;
                ValorXLbl.Visibility = Visibility.Visible;
                ValorXTxt.Visibility = Visibility.Visible;
            }
        }

        private void IngresarPuntosBtn_Click(object sender, RoutedEventArgs e)
        {
            A0Txt.Text = "";
            A1Txt.Text = "";
            CorrelacionTxt.Text = "";
            GradoTxt.Text = "";
            ValorXTxt.Text = "";
            PolinomioTxt.Text = "";
            var cantidad = Convert.ToInt32(PuntosTxt.Text);
            var indice = 2;
            const int Width = 80;
            const int Height = 25;
            GridTabla.ColumnDefinitions.Clear();
            GridTabla.RowDefinitions.Clear();
            GridTabla.Children.Clear();
            GridTabla.Width = Width * (cantidad + 1);
            GridTabla.Height = Height * (cantidad + 1);

            for (int j = 0; j <= cantidad; j++)
            {
                GridTabla.RowDefinitions.Add(new RowDefinition { Height = new GridLength(25) });

                for (int i = 0; i < 2; i++)
                {
                    GridTabla.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(80) });

                    if (j == 0)
                    {

                        var label = new TextBox();
                        label.IsReadOnly = true;
                        label.IsTabStop = false;
                        label.Background = Brushes.Turquoise;
                        if (i == 0)

                            label.Text = "X";
                        else
                            label.Text = "Y";

                        Grid.SetColumn(label, i);
                        Grid.SetRow(label, j);
                        GridTabla.Children.Add(label);
                    }
                    else
                    {
                        var text = new TextBox
                        {
                            Height = Height,
                            Width = Width,
                        };
                        text.TabIndex = indice + 1;
                        Grid.SetColumn(text, i);
                        Grid.SetRow(text, j);
                        GridTabla.Children.Add(text);

                    }

                }

            }
        }

        private void IngresarTablaBtn_Click(object sender, RoutedEventArgs e)
        {

            int cantidad = Convert.ToInt32(PuntosTxt.Text);
            double[,] matriz = new double[cantidad, 2];
            var fila = 0;
            var cultura = CultureInfo.CreateSpecificCulture("en-US");
            for (int i = 1; i <= cantidad; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    var hijo = GridTabla.Children.Cast<TextBox>().First(x => Grid.GetRow(x) == i && Grid.GetColumn(x) == j);
                    var valor = hijo.Text;
                    matriz[fila, j] = Convert.ToDouble(valor, cultura);

                }
                fila = fila + 1;
            }


            if (Metodo == MetodoADC.RegresionLineal)
            {
                var parametros = new ParametrosRegresionLineal() { Tabla = matriz, CantidadPuntos = cantidad };

                var resultado = new ResultadoAjusteDeCurvas();


                resultado = metodoAjusteDeCurvas.MetodoRegresionLineal(parametros);

                A0Txt.Text = resultado.valores[0].ToString("N7");
                A1Txt.Text = resultado.valores[1].ToString("N7");
                CorrelacionTxt.Text = resultado.CoeficienteCorrelacion.ToString();

            }
            else
            {
                if (Metodo == MetodoADC.RegresionPolinomial)
                {

                    if (String.IsNullOrEmpty(GradoTxt.Text))
                    {
                        MessageBox.Show("El grado no puede estar vacio", "Advertencia", MessageBoxButton.OK);
                    }
                    else
                    {


                        int grado = Convert.ToInt32(GradoTxt.Text);
                        var parametros = new ParametrosRegresionPolinomial() { Tabla = matriz, Grado = grado };



                        var resultado = new ResultadoAjusteDeCurvas();
                        resultado = metodoAjusteDeCurvas.MetodoRegresionPolinomial(parametros);

                        var mensaje = "";
                        for (int i = 0; i < resultado.valores.Length; i++)
                        {
                            mensaje = mensaje + "\r\n" + ("a" + (i).ToString() + " = " + resultado.valores[i].ToString(cultura) + " ");

                        }

                        mensaje = mensaje + "\r\n" + "Correlacion=" + resultado.CoeficienteCorrelacion.ToString();

                        MessageBox.Show(mensaje, "Resultado", MessageBoxButton.OK);
                    }
                }
                else
                {
                    if (String.IsNullOrEmpty(GradoTxt.Text))
                    {
                        MessageBox.Show("El X a evaluar no puede estar vacio", "Advertencia", MessageBoxButton.OK);
                    }
                    else
                    {
                        int x = Convert.ToInt32(GradoTxt.Text);
                        var parametros = new ParametrosLagrange() { Tabla = matriz, X = x };
                        var resultado = new ResultadoLagrange();
                        resultado = metodoAjusteDeCurvas.MetodoLagrange(parametros);
                        PolinomioTxt.Text = resultado.Polinomio;
                        ValorXTxt.Text = resultado.Resultado.ToString(cultura);
                    }
                }
            }
        }
        private void ValorXTxt_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
