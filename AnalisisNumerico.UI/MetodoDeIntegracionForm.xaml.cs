﻿using AnalisisNumerico.Entidades.IntegracionNumerica;
using System;
using System.Globalization;
using System.Windows;


namespace AnalisisNumerico.UI
{
    /// <summary>
    /// Lógica de interacción para MetodoDeIntegracionForm.xaml
    /// </summary>
    /// 
    public enum MetodoIN
    {
        Trapecio,
        TrapecioMultiple,
        Simpson1_3,
        Simpson1_3Multiple,
    }
    public partial class MetodoDeIntegracionForm : Window
    {
        public MetodoIN Metodo;
        private readonly IMetodoIntegracionNumerica metodoIntegracionNumerica;

        public MetodoDeIntegracionForm(IMetodoIntegracionNumerica metodo)
        {
            this.metodoIntegracionNumerica = metodo;
            InitializeComponent();
        }
        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            if (Metodo != MetodoIN.Trapecio && Metodo != MetodoIN.Simpson1_3)
            {
                CantidadIntervalosLbl.Visibility = Visibility.Visible;
                CantidadPuntosTxt.Visibility = Visibility.Visible;
            }
        }

        private void IngresarBtn_Click(object sender, RoutedEventArgs e)
        {
            var culture = CultureInfo.CreateSpecificCulture("en-US");
            var funcion = FuncionTxt.Text;
            var puntoInicio = Convert.ToDouble(PuntoInicioTxt.Text, culture);
            var puntoFin = Convert.ToDouble(PuntoFinalTxt.Text, culture);
            var parametros = new ParametrosMetodosSimples() { Funcion = funcion, PuntoInicial = puntoInicio, PuntoFinal = puntoFin };
            var resultado = 0.0;
            var cantidadIntervalos = 0;
            if (!String.IsNullOrEmpty(CantidadPuntosTxt.Text))
            {
                cantidadIntervalos = Convert.ToInt32(CantidadPuntosTxt.Text, culture);
            }

            switch (Metodo)
            {
                case MetodoIN.Trapecio:
                    resultado = metodoIntegracionNumerica.MetodoTrapecio(parametros);
                    AreaTxt.Text = resultado.ToString("N7");
                    break;
                case MetodoIN.TrapecioMultiple:
                    var parametrosMultiples = new ParametrosMetodosMultiples { PuntoInicial = parametros.PuntoInicial, PuntoFinal = parametros.PuntoFinal, CantidadIntervalos = cantidadIntervalos, Funcion = parametros.Funcion };
                    resultado = metodoIntegracionNumerica.MetodoTrapecioMultiple(parametrosMultiples);
                    AreaTxt.Text = resultado.ToString("N7");
                    break;
                case MetodoIN.Simpson1_3:

                    resultado = metodoIntegracionNumerica.MetodoSimpson1_3Simple(parametros);
                    AreaTxt.Text = resultado.ToString("N7");
                    break;
                case MetodoIN.Simpson1_3Multiple:
                    parametrosMultiples = new ParametrosMetodosMultiples { PuntoInicial = parametros.PuntoInicial, PuntoFinal = parametros.PuntoFinal, CantidadIntervalos = cantidadIntervalos, Funcion = parametros.Funcion };
                    resultado = metodoIntegracionNumerica.MetodoSimpson(parametrosMultiples);
                    AreaTxt.Text = resultado.ToString("N7");
                    break;
            }

        }
    }
}
