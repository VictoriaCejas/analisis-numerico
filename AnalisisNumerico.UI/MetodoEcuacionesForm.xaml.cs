﻿using AnalisisNumerico.Entidades.Ecuaciones;
using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace AnalisisNumerico.UI
{
    public enum MetodosEcuaciones
    {
        GaussJordan,
        GaussSeidel,
    }
    public partial class MetodoEcuacionesForm : Window
    {
        public MetodosEcuaciones Metodo { get; set; }
        private readonly IMetodoEcuaciones metodoEcuaciones;


        public MetodoEcuacionesForm(IMetodoEcuaciones metodoEcuaciones)
        {
            InitializeComponent();
            this.metodoEcuaciones = metodoEcuaciones;
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);

            if (Metodo == MetodosEcuaciones.GaussJordan)
            {
                errorTxt.Visibility = Visibility.Hidden;
                errorLbl.Visibility = Visibility.Hidden;
                iteracionesLbl.Visibility = Visibility.Hidden;
                iteracionesTxt.Visibility = Visibility.Hidden;
            }
        }

        private void IngresarAMatrizCoeficientesBtn_Click(object sender, RoutedEventArgs e)
        {

            var cantidad = Convert.ToInt32(IncognitasTxt.Text);
            var indice = 2;
            const int Width = 80;
            const int Height = 25;
            CoeficientesGrid.ColumnDefinitions.Clear();
            CoeficientesGrid.RowDefinitions.Clear();
            CoeficientesGrid.Children.Clear();
            CoeficientesGrid.Width = Width * (cantidad + 1);
            CoeficientesGrid.Height = Height * (cantidad + 1);
            for (int j = 0; j <= cantidad; j++)
            {
                CoeficientesGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(25) });

                for (int i = 0; i <= cantidad; i++)
                {
                    CoeficientesGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(80) });

                    if (j == 0)
                    {
                        var label = new TextBox();
                        label.IsReadOnly = true;
                        label.IsTabStop = false;
                        label.Background = Brushes.Turquoise;
                        if (i == cantidad)
                            label.Text = "Resultado";
                        else
                            label.Text = "X" + (i + 1).ToString();

                        Grid.SetColumn(label, i);
                        Grid.SetRow(label, j);
                        CoeficientesGrid.Children.Add(label);
                    }
                    else
                    {
                        var text = new TextBox
                        {
                            Height = Height,
                            Width = Width,
                        };
                        text.TabIndex = indice + 1;
                        Grid.SetColumn(text, i);
                        Grid.SetRow(text, j);
                        CoeficientesGrid.Children.Add(text);
                    }
                }
            }
        }

        private void IngresarEcuacionTxt_Click(object sender, RoutedEventArgs e)
        {
            //INGRESAR CON PUNTOS.
            int cantidad = Convert.ToInt32(IncognitasTxt.Text);
            double[,] matriz = new double[(cantidad), (cantidad + 1)];
            var fila = 0;
            var cultura = CultureInfo.CreateSpecificCulture("en-US");
            for (int i = 1; i <= (cantidad); i++)
            {
                for (int j = 0; j <= cantidad; j++)
                {
                    var hijo = CoeficientesGrid.Children.Cast<TextBox>().First(x => Grid.GetRow(x) == i && Grid.GetColumn(x) == j);
                    var valor = 0.0;
                    string[] aux;
                    if (hijo.Text.Contains("/"))
                    {
                        aux = hijo.Text.Split(new char[] { '/' }, 2);
                        valor = double.Parse(aux[0], cultura) / double.Parse(aux[1], cultura);
                    }
                    else
                    {
                        valor = double.Parse(hijo.Text, cultura);
                    }

                    matriz[fila, j] = valor;
                }

                fila = fila + 1;
            }

            var resultado = "";
            switch (this.Metodo)
            {
                case MetodosEcuaciones.GaussJordan:
                    var parametos = new ParametrosGaussJordan { Matriz = matriz, CantidadIncognitas = cantidad };
                    var gaussJordan = metodoEcuaciones.MetodoGaussJordan(parametos);
                    resultado = CrearMensajeGaussJordan(gaussJordan.Resultados, gaussJordan.Resultados.Length);
                    break;
                case MetodosEcuaciones.GaussSeidel:
                    double tolerancia = 0.0001;
                    var iteraciones = 100;

                    if (!(string.IsNullOrEmpty(iteracionesTxt.Text)))
                    {
                        iteraciones = Convert.ToInt32(iteracionesTxt.Text);
                    }
                    if (!(string.IsNullOrEmpty(errorTxt.Text)))
                    {
                        tolerancia = Convert.ToDouble(errorTxt.Text, cultura);
                    }

                    var parametros = new ParametrosGaussSeidel { Matriz = matriz, CantidadIncognitas = cantidad, Iteraciones = iteraciones, Tolerancia = tolerancia };
                    var gaussSeidel = metodoEcuaciones.MetodoGaussSeidel(parametros);
                    resultado = CrearMensajeGaussSeidel(gaussSeidel.Resultados, gaussSeidel.ErrorRelativo, gaussSeidel.Iteraciones);
                    break;

            }
            MessageBox.Show(resultado, "Resultado", MessageBoxButton.OK);
        }

        public string CrearMensajeGaussSeidel(double[] solucion, double errorRelativo, int iteraciones)
        {
            string mensaje = "";
            var cantidadIncognitas = solucion.Length;

            mensaje = CrearMensajeGaussJordan(solucion, cantidadIncognitas);
            mensaje = mensaje + "\r\n" + "El error relativo es = " + errorRelativo.ToString("N10");
            mensaje = mensaje + "\r\n" + "Las iteraciones fueron = " + iteraciones.ToString();

            return mensaje;
        }

        public string CrearMensajeGaussJordan(double[] resutados, int cantidadIncognitas)
        {
            CultureInfo culture = new CultureInfo("en-Us");
            string mensaje = "";
            var numero = "";

            for (int indice = 0; indice < cantidadIncognitas; indice++)
            {
                numero = resutados[indice].ToString("N7");
                if (indice > 0)
                    mensaje = mensaje + "\r\n" + ("X" + (indice + 1).ToString() + " = " + (numero) + " ");
                else
                    mensaje = ("X" + (indice + 1).ToString() + " = " + (numero) + " ");
            }
            return mensaje;
        }

    }
}
