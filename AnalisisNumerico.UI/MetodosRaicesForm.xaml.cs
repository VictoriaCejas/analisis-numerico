﻿using AnalisisNumerico.Entidades;
using AnalisisNumerico.Entidades.Raices;
using System;
using System.Globalization;
using System.Windows;

namespace AnalisisNumerico.UI
{
    public enum MetodosRaices
    {
        Biseccion,
        ReglaFalsa,
        Tangente,
        Secante,
    }
    /// <summary>
    /// Lógica de interacción para BiseccionForm.xaml
    /// </summary>
    public partial class MetodosRaicesForm : Window
    {
        public MetodosRaices Metodo { get; set; }
        private readonly IMetodosRaices metodosRaices;

        public MetodosRaicesForm(IMetodosRaices metodosRaices)
        {
            InitializeComponent();
            this.metodosRaices = metodosRaices;
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);

            if (Metodo == MetodosRaices.Tangente)
            {
                limiteSuperiorTxt.Visibility = Visibility.Hidden;
                limiteSuperiorLbl.Visibility = Visibility.Hidden;
                limiteInferiorLbl.Content = "Punto de inicio";
            }
        }

        private void btnCalcular_Click(object sender, RoutedEventArgs e)
        {
            //INGRESAR TODO CON PUNTOS
            var cultura = CultureInfo.CreateSpecificCulture("en-US");
            double tolerancia = 0.0001;
            var iteraciones = 100;

            var funcion = funcionTxt.Text;
            var xi = Convert.ToDouble(limiteInferiorTxt.Text, cultura);
            var xd = 0.0;
            if (!(string.IsNullOrEmpty(limiteSuperiorTxt.Text)))
            {
                xd = Convert.ToDouble(limiteSuperiorTxt.Text, cultura);
            }
            if (!(string.IsNullOrEmpty(iteracionesTxt.Text)))
            {
                iteraciones = Convert.ToInt32(iteracionesTxt.Text);
            }
            if (!(string.IsNullOrEmpty(toleranciaTxt.Text)))
            {
                tolerancia = Convert.ToDouble(toleranciaTxt.Text, cultura);
            }

            var titulo = this.Title.ToString();
            var esBiseccion = titulo == "Biseccion";

            try
            {

                var parametros = new ParametrosBiseccion
                {
                    Funcion = funcion,
                    Xi = xi,
                    Xd = xd,
                    Tolerancia = tolerancia,
                    Iteraciones = iteraciones,
                    EsBiseccion = esBiseccion,
                };

                var resultado = new ResultadoRaices();

                switch (this.Metodo)
                {
                    case MetodosRaices.Biseccion:
                        resultado = metodosRaices.MetodoBiseccion(parametros);
                        break;
                    case MetodosRaices.ReglaFalsa:
                        resultado = metodosRaices.MetodoReglaFalsa(parametros);
                        break;
                    case MetodosRaices.Tangente:
                        resultado = metodosRaices.MetodoTangente(new ParametrosTangente
                        {
                            Funcion = funcion,
                            Xi = xi,
                            Tolerancia = tolerancia,
                            Iteraciones = iteraciones
                        });
                        break;
                    case MetodosRaices.Secante:
                        resultado = metodosRaices.MetodoSecante(new ParametrosSecante
                        {
                            Funcion = funcion,
                            Xi = xi,
                            Xd = xd,
                            Tolerancia = tolerancia,
                            Iteraciones = iteraciones
                        });
                        break;
                }
                this.resultadoRaizTxt.Text = resultado.Raiz.ToString();
                this.resultadoErrorTxt.Text = resultado.ErrorRelativo.ToString();
                this.resultadoIteracionesTxt.Text = resultado.Iteraciones.ToString();

            }
            catch (ArgumentException error)
            {
                MessageBox.Show(error.Message, "Error", MessageBoxButton.OK);
            }
        }
    }
}
