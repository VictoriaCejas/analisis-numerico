﻿using AnalisisNumerico.Entidades.AjustesDeCurvas;
using AnalisisNumerico.Entidades.Ecuaciones;
using AnalisisNumerico.Entidades.IntegracionNumerica;
using AnalisisNumerico.Entidades.Raices;
using AnalisisNumerico.Logica;
using SimpleInjector;
using System;

namespace AnalisisNumerico.UI
{
    class Program
    {
        internal static Container container;

        [STAThread]
        static void Main()
        {
            container = Bootstrap();

            // Any additional other configuration, e.g. of your desired MVVM toolkit.

            RunApplication(container);
        }

        private static Container Bootstrap()
        {
            // Create the container as usual.
            var container = new Container();
            // Register your windows and view models:
            container.Register<InicioForm>();
            container.Register<MetodosRaicesForm>();
            container.Register<MetodoEcuacionesForm>();
            container.Register<MetodoAjusteDeCurvasForm>();
            container.Register<MetodoDeIntegracionForm>();
            container.Register<IMetodosRaices, MetodoRaices>();
            container.Register<IMetodoEcuaciones, MetodoEcuaciones>();
            container.Register<IMetodoAjusteDeCurvas, MetodoAjusteDeCurvas>();
            container.Register<IUtilityFunctions, UtilityFunctions>();
            container.Register<IMetodoIntegracionNumerica, MetodoIntegracionNumerica>();
            return container;
        }

        private static void RunApplication(Container container)
        {
            try
            {
                var app = new App();
                var mainWindow = container.GetInstance<InicioForm>();
                app.Run(mainWindow);
            }
            catch (Exception ex)
            {
                //Log the exception and exit
            }
        }
    }
}
